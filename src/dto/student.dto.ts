export class createStudentDto {
        id:string;
        name:string;
        teacher:string
}
export class updateStudentDto{
    name:string;
    teacher:string;
}
export class findStudentResponseDto{
    name:string;
    teacher:string;
}
export class StudentResponseDto{
    id:string
    name:string;
    teacher:string;
}