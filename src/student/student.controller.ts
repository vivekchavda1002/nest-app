import { Body, Controller,ParseUUIDPipe, Get, Param, Post, Put } from '@nestjs/common';
import { createStudentDto, findStudentResponseDto, StudentResponseDto, updateStudentDto } from 'src/dto/student.dto';
import { StudentService } from './student.service';


@Controller('student')
export class StudentController {
   constructor(private readonly studentService:StudentService){} 
    @Get()
    getStudent():findStudentResponseDto[]{
        return this.studentService.getStudents()
    }
    @Get(':id')
    getStudentById(@Param('id',new ParseUUIDPipe()) id:string):findStudentResponseDto{
         return this.studentService.getStudentById(id)
    }
    @Post()
    createStudent(@Body() body:createStudentDto){
        return this.studentService.createStudent(body)
    }

    @Put(':id')
    updateStudent(@Param('id',new ParseUUIDPipe()) id:string,@Body() body:updateStudentDto){
        return this.studentService.updateStudent(body,id)
    }

}
  