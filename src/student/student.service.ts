import { Injectable } from '@nestjs/common';
import { students } from 'src/db';
import {v4 as uuid} from 'uuid'
import { createStudentDto, findStudentResponseDto, StudentResponseDto, updateStudentDto } from 'src/dto/student.dto';

@Injectable()
export class StudentService {
    private students = students
    getStudents():findStudentResponseDto[]{
        return this.students;
    }
    getStudentById(id:string):findStudentResponseDto{
        return this.students.find((student)=>student.id === id)
    }
    createStudent(data:createStudentDto):createStudentDto{
       let newStudent = {
           id:uuid(),
           ...data
       }
       this.students.push(newStudent)
       return newStudent;
    }

    updateStudent(payload: updateStudentDto, id: string): StudentResponseDto {
        let updatedStudent: StudentResponseDto
        let updatedStudentList = this.students.map(student => {
            if(student.id === id){
                updatedStudent = {
                    id,
                    ...payload
                };
                return updatedStudent
            } else return student
        });
        this.students = updatedStudentList
        return updatedStudent
    }
    
}
