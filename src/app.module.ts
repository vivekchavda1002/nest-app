import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StudentController } from './student/student.controller';
import { TeacherController } from './teacher/teacher.contorller';
import { StudentTeacherController } from './teacher/student.controller';
import { StudentService } from './student/student.service';
import { TeacherService } from './teacher/teacher.service';
import { validstudentMiddleware } from './common/middleware/studentMiddleware';

@Module({
  imports: [],
  controllers: [
    AppController,
    StudentController,
    TeacherController,
    StudentTeacherController
    
  ],
  providers: [AppService, StudentService, TeacherService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(validstudentMiddleware).forRoutes({
      path:'student/:studentId',
      method:RequestMethod.GET
    });
    consumer.apply(validstudentMiddleware).forRoutes({
      path:'student/:studentId',
      method:RequestMethod.PUT
    });

  }
}
