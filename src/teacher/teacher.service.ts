import { Injectable } from '@nestjs/common';
import { teachers,students } from 'src/db';
import { findStudentResponseDto, StudentResponseDto } from 'src/dto/student.dto';
import { FindTeacherResponseDto } from './dto/teacher.dto';
@Injectable()
export class TeacherService {

   private teachers = teachers
   private students = students
    getTeacher():FindTeacherResponseDto[]{
        return this.teachers
    }
    getTeacherbyId(id:string){
        return this.teachers.find((teacher)=>teacher.id === id)
    }
    
    getStudentsByTeacherId(teacherId: string): findStudentResponseDto[] {
        return this.students.filter(student => {
            return student.teacher === teacherId
            
        })
    }
    
    updateStudentTeacher(studentId:string,teacherId:string){
        let updatedStudent: StudentResponseDto
        let updatedStudentList = this.students.map(student => {
            if(student.id === studentId){
                updatedStudent = {
                   ...student,
                   teacher:teacherId
                };
                return updatedStudent
            } else return student
        });
        this.students = updatedStudentList
        return updatedStudent
    }


}
