import { Controller, Get, Param, Put, ParseUUIDPipe } from '@nestjs/common';
import { findStudentResponseDto, StudentResponseDto } from 'src/dto/student.dto';
import { TeacherService } from './teacher.service';
@Controller('teachers/:teacherId/students')
export class StudentTeacherController {
    constructor(private readonly teacherService:TeacherService){}
    @Get()
    getStudents(
        @Param('teacherId') teacherId: string 
    ): findStudentResponseDto[] {
        return this.teacherService.getStudentsByTeacherId(teacherId)
    }

    @Put('/:studentId',)
    updateStudentTeacher(@Param('studentId',new ParseUUIDPipe()) studentId:string,@Param('teacherId') teacherId:string):StudentResponseDto {
        return this.teacherService.updateStudentTeacher(teacherId,studentId);
    }
}